def __log_result_of_assigner(task_name, status_code, detail):
    # if status code not equal to 200 notify the system admins
    record = {
        "task_name": task_name,
        "status_code": status_code,
        "detail": detail
    }
