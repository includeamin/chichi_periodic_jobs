import requests
from Logger.Logger import __log_result_of_assigner

assigner_url = 'http://chichiapp.ir:30034/'


def assign_unassigned_package():
    result = requests.get(assigner_url)
    __log_result_of_assigner("product_capacity_reserved_timing", result.status_code, result.json())
